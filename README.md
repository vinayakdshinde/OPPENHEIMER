# OPPENHEIMER Automation

This is the test automation for the Oppenheimer Project using robot framework.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Application under test Setup

1. Check-out code from https://github.com/strengthandwill/oppenheimer-project-dev
2. Run below command to start AUT
	java -jar OppenheimerProjectDev.jar  
2. Application URL `APP_URL` in variables.py is set to correct URL

## Automation Framework Pre-requisites

1. Install google chrome
2. Install Python,
	https://www.python.org/downloads
3. Install Java 
	https://www.java.com/download/ie_manual.jsp
4. Install Robot framework and dependecies using below command,
	pip install -r installerlist.txt

## How to run the tests

#### Running a single test
`	python -m robot -d results .\testcases\<test_name>.robot`
#### Running the whole test suites
`	python -m robot -d results .\testcases\`
#### Running the test by tags
`	python -m robot -d results -i <tag_name> .\testcases\`


