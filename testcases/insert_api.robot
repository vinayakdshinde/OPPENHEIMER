*** Settings ***
Documentation   This tests the functions associated with the role/s of the clerk  using the API
Library     RequestsLibrary
Library     Collections
Library     SeleniumLibrary
Library     OperatingSystem
Resource    ../libraries/common.robot
Variables   ../libraries/variables.py
Test Setup  common.Rake database
Test Teardown   common.Close page

*** Variables ***
${upload_files_path}    ${EXECDIR}\\testdata

*** Test Cases ***
Add single valid json data to the database via API
    [Tags]  Acceptance
    Add single entry to database via API and verify in UI	${upload_files_path}\\single_entry.json     202     success 

Add single invalid json data to the database via API
    [Tags]  Acceptance
    Add single entry to database via API and verify in UI	${upload_files_path}\\single_invalid.json   500     error 

Add single invalid data to the database via API
    [Tags]  Acceptance
    Add single entry to database via API and verify in UI	${upload_files_path}\\text_file.txt     400     error
	
Add multiple valid json data to the database via API
    [Tags]  Acceptance
    Add multiple entries to database via API and verify in UI	${upload_files_path}\\multiple_entries.json     202     success    

Add multiple invalid json data to the database via API
    [Tags]  Acceptance
    Add multiple entries to database via API and verify in UI	${upload_files_path}\\multiple_invalid.json     500     error     

Add multiple invalid data to the database via API
    [Tags]  Acceptance
    Add multiple entries to database via API and verify in UI	${upload_files_path}\\text_file.txt     400      error      
    
	

*** Keywords ***
Add single entry via API
    [Arguments]     ${single_entry_json_file}   ${expected_response_code}
    [Documentation]     Adds a single entry to the database
    [Tags]      Acceptancce
    ${body}=    Get file    ${single_entry_json_file}
    ${header}=  Create Dictionary   Content-Type=application/json
    ${response}=    Run Keyword and Ignore Error    POST     ${single_entry_url}      headers=${header}   data=${body}
	Log		${response}
    Status Should Be    ${expected_response_code}

Add single entry to database via API and verify in UI
    [Arguments]     ${single_entry_file}   ${response_code}   ${status}     
    IF    "${status}" == "success"
        ${json_data}=   data_fetch.Get data from JSON file    ${single_entry_file }
		Log		${json_data}
        ${relief_list}=    common.Get relief list computation from given data    ${json_data}     False
    END
    Add single entry via API    ${single_entry_file}    ${response_code}
    IF    "${status}" == "success"
        Verify relief list table in UI    ${status}   ${relief_list}
    END

Verify relief list table in UI
    [Arguments]     ${status}   ${expected_relief_list}
    common.Login to page
    common.Refresh tax relief table
    IF    "${status}" == "success"
        common.Wait table caption
        Log To Console    Verify tax relief table headers
        FOR    ${header}    IN    @{table_headers}
            common.Verify relief list table header  ${header}
        END
        ${table_data}=  common.Get relief list in table
        Lists Should Be Equal    ${table_data}    ${expected_relief_list}
    ELSE
        common.Wait empty table message
    END

Add multiple entries to database
    [Arguments]     ${multiple_entries_json_file}   ${expected_response_code}
    [Documentation]     Adds multiple entries to the database
    [Tags]      Acceptance
    ${body}=    Get file    ${multiple_entries_json_file}
    ${header}=  Create Dictionary   Content-Type=application/json
    ${response}=    Run Keyword and Ignore Error    POST      ${multiple_entry_url}      headers=${header}   data=${body}
    Status Should Be    ${expected_response_code}

Add multiple entries to database via API and verify in UI
    [Arguments]     ${multiple_entries_file}   ${response_code}   ${status}

    IF    "${status}" == "success"
        ${json_data}=   data_fetch.Get data from JSON file    ${multiple_entries_file}
        ${relief_list}=    common.Get relief list computation from given data    ${json_data}   False
	END
    Add multiple entries to database    ${multiple_entries_file}    ${response_code}
    IF    "${status}" == "success"
       Verify relief list table in UI    ${status}   ${relief_list}
    END