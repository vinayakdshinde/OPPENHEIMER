
*** Settings ***
Documentation   This tests the functions associated with the role/s of the clerk using the UI
Library         Collections
Library         SeleniumLibrary
Library         OperatingSystem
Resource    ../libraries/common.robot
Variables   ../libraries/variables.py
Test Setup      common.Rake database
Test Teardown   common.Close page

*** Variables ***
${upload_files_path}    ${EXECDIR}\\testdata\\

*** Test Cases ***          FILE_PATH                                 STATUS                        
Valid CSV file 
    [Tags]  Acceptance
    [Template]  Upload CSV file to portal
    ${upload_files_path}sample_valid.csv      success            

Invalid CSV file  
    [Tags]  Acceptance
    [Template]  Upload CSV file to portal
    ${upload_files_path}sample_invalid.csv    error              

Large CSV file  
    [Tags]  Acceptance
    [Template]  Upload CSV file to portal
    ${upload_files_path}sample_large.csv      success        

Invalid file type
    [Tags]  Acceptance
    [Template]  Upload CSV file to portal
    ${upload_files_path}text_file.txt         error              

*** Keywords ***
Upload CSV file to portal
    [Arguments]     ${file_path}    ${status}      
    [Documentation]     Uploads a valid CSV file from UI
    [Tags]  Acceptance
    ${upload_data}=    common.Convert CSV to JSON  ${file_path}
    IF    "${status}" == "success"
         ${relief_list}=    common.Get relief list computation from given data    ${upload_data}     False
    END
    common.Login to page
    common.Verify text of element    ${upload_csv_section_label_xpath}    ${upload_csv_section_label}
    Upload file    ${file_path}
    common.Refresh tax relief table
    IF    "${status}" == "success"
        common.Wait table caption
        Log To Console    Verify tax relief table headers
        FOR    ${header}    IN    @{table_headers}
            common.Verify relief list table header  ${header}
        END
        ${table_data}=  common.Get relief list in table
        Lists Should Be Equal    ${table_data}    ${relief_list}
    ELSE
        common.Wait empty table message
    END


Upload file
    [Arguments]    ${file_to_upload}
    Choose File    ${upload_csv_file_xpath}   ${file_to_upload}


