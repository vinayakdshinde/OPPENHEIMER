##### Clear Database Record #######

curl -i -X POST -H 'Content-Type: application/json' http://localhost:8080/calculator/rakeDatabase

#### Single record API command.#########
curl -H "Content-Type: application/json" --data @single_entry.json http://localhost:8080/calculator/insert
curl -H "Content-Type: application/json" --data @single_invalid.json http://localhost:8080/calculator/insert
curl -H "Content-Type: application/json" --data @text_file.txt http://localhost:8080/calculator/insert

#### Multiple record API command.#########

curl -H 'curl -H "Content-Type: application/json" --data @multiple_entries.json  http://localhost:8080/calculator/insertMultiple
curl -H 'curl -H "Content-Type: application/json" --data @multiple_invalid.json  http://localhost:8080/calculator/insertMultiple
curl -H 'curl -H "Content-Type: application/json" --data @text_file.txt  http://localhost:8080/calculator/insertMultiple


#### Tax Relief report ##############

curl -H 'curl -H "Content-Type: application/json" http://localhost:8080/calculator/taxRelief